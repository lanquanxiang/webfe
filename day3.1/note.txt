css优点：
	1、减少重复代码，提高工作效率；
	2、易维护易扩展；
	3、内容和表现分离。

CSS语法规则：
	选择器 {声明}  声明= 属性：属性值；
	选择器 {属性:属性值;}
	h1 {color:red;}
	
CSS使用规则：
	1、内部样式 style标签（放在网页的头部head）
	2、外部样式 link标签导入CSS文件
	<link rel="stylesheet" href="css/myCSS.css" />
	3、内联样式（放在元素开标签中，用style属性进行装饰）

CSS层叠样式表:
	1、浏览器缺省样式（低）
	2、外部样式
	3、内部样式
	4、内联样式（高）
	
CSS选择器
	1、基本选择器：类型选择器E  id选择器 #  class选择器 . 
	2、派生选择器
		E#myid  E.waring
	3、组合选择器
		E F （后代选择器）
		E>F (父子选择器)
		E+F （相邻兄弟选择器）
		E~F (通用兄弟选择器)
	4、用户操作伪类选择器 E：用户操作
		爱恨原则 love hate   link visited hover active
	5、伪元素选择器
	6、属性选择器
	7、结构伪类选择器
	
CSS属性
	1、字体属性
		
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	